%define BUFFER_SIZE 255
%define OFFSET 8
%include "lib.inc"
%include "words.inc"


section .rodata
    buffer_size_msg: db "Error! Buffer max size exceeded\n",0         ; Зададим сообщения об ошибке
    no_key_msg: db "Error! Key wasn't found\n",0



global _start
extern find_word
section .text                                                       
    _start:
        sub rsp, BUFFER_SIZE                                        ; Создадим буффер на стэке
        mov rdi, rsp                                                ; Передадим в качестве аргументов функции read_word начало буффера и его размер
        mov rsi, BUFFER_SIZE
        call read_word                                              ; Прочитаем слово в буффер
        test rax,rax                                                ; Если возникла ошибка по переполнению буффера - перейдем к ее обработке
        jz .buffer_size_err                                          
        mov rdi, rsp                                                ; Передадим в качестве аргументов функции find_word начало буффера и указатель на последний элемент
        mov rsi, pointer
        call find_word                                              ; Найдем слово
        test rax,rax                                                ; Если мы не нашли такого ключа, перейдем к обработке этой ошибки
        jz .no_key_err  
        lea rdi, [rax+OFFSET]                                       ; Иначе у нас есть адрес элемента словаря с подходящим ключем, перейдем на адрес начала ключа                                             ; Пропустим указатель
        push rdi
        call string_length                                          ; Считаем длину ключа
        pop rdi
        add rdi, rax                                                ; Пропустим ключ
        inc rdi                                                     ; И нуль-терминатор ключа
        call print_string                                           ; Выведем значение
        add rsp, BUFFER_SIZE                                        ; Вернем стэк в исходное состояние
        call exit                       
        

        .buffer_size_err:
            mov rdi, buffer_size_msg                                ; Передадим сообщения об ошибке
            jmp .print_err
        .no_key_err:
            mov rdi, no_key_msg
        .print_err:
            add rsp, BUFFER_SIZE                                    ; Вернем стэк в исходное состояние
            call print_error                                        ; Вызов функции для вывода ошибки
            call exit
